import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import CallScreen from '../video/CallScreen'
import LogScreen from '../video/LogScreen'
import {Livechat2Api} from 'react-native-livechatav/sig/livechat2api'
import {Logger} from 'react-native-livechatav/sig/livechat2'
import StartScreen from './StartScreen'
import SettingsScreen from '../video/SettingsScreen'

import {configs} from '../config'

import {createSwitchNavigator, createAppContainer} from 'react-navigation';

const NavApp = createSwitchNavigator(
    { 
            Start : StartScreen,
            Login : StartScreen,
            Call :  CallScreen,
            Logs : { screen: LogScreen },
            ChatThread : { screen: LogScreen },
            ChatList : { screen: LogScreen },
            Settings: { screen : SettingsScreen }
    },
    {
            //defaultNavigationOptions : { tabBarVisible: false },
            initialRouteName : 'Start',
            //lazy : false
    }
);

const NavAppCont = createAppContainer(NavApp);

export default class TestApp extends React.Component {

  constructor(props)
  {
    super(props);
    window.livechat2Api = new Livechat2Api( configs.selected.webSocketUrl );
   
    
    Logger.info = ( m )=>{ global.log.func( "info", m ); };
    //Logger.debug = ( m )=>{ global.log.func( "debug", m ); };
    Logger.error = ( m )=>{ global.log.func( "error", m ); };
    Logger.warn = ( m )=>{ global.log.func( "warn", m ); };
    

    console.log( "Config Obj = " + configs.selected.webSocketUrl /*JSON.stringify(confObj)*/ );
  }

  render() {
    return (
      <NavAppCont/>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
