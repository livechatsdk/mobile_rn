import React, {Component} from 'react';
import {
    FlatList,
    Platform,
    Alert,
    StyleSheet,
    Text,
    View,
    Button,
    Image,
    TouchableOpacity,
    Modal,
    TextInput
} from 'react-native';
import AppButton from '../video/AppButton';
import AVChatConnection from 'react-native-livechatav/av/LivechatAv';
import {createStore} from 'redux';
import {reducer} from 'redux-form';
import {Livechat2Api} from 'react-native-livechatav/sig/livechat2api';
import SingleClickExample from './SingleClickExample';

import {configs} from '../config';
import {Input} from 'native-base';


function logi(msg) {
    global.log.func("info", msg);
    return msg;
}

function logw(msg) {
    global.log.func("warn", msg);
    return msg;
}

function loge(msg) {
    global.log.func("error", msg);
    return msg;
}

function logd(msg) {
    global.log.func("debug", msg);
    return msg;
}

/*
reducer = ( state, action ) =>
{
    switch( action.type )
    {
        case "startsig":
            return { ctlbtn:{ txt:"End", isActive:true, click: this.end } }
    }
}
let store = createStore()
*/

export default class StartScreen extends Component {

    state = {
        ctlbtn: {txt: "Start", isActive: false, click: this.start},
        callbtn: {txt: "Call", isActive: false},
        reqbtn: {isActive: false},
        libver: "",
        findUserResp: {},
        userid: "",
        sid: "",
        txtList: [],
        rqskill: "Banking",
        rqparam: "{ \"name\":\"Nuwan\" }",
        isFoundEWTStatus: false,
        requestId: null,
        EWTStatus: null,
        isUserFound: false,
        agentUserId: 0
    }

    //libver="";
    api;

    constructor(props) {
        super(props);

        var c = [];
        c["recv"] = "pink";
        c["sent"] = "lightgreen";
        this.itemBgcolor = c;

        logi("StartScreen constructed");

        this.orgSetState = this.setState;
        this.setState = (f) => {
            this.orgSetState((s) => {
                    return global.state.StartScreen = f(s);
                }
            );
        }

        AVChatConnection.getLibVersion((val) => {
            this.setState((s) => {
                s.libver = val;
                return s;
            });
        });

        if (global.state.StartScreen) {
            logi("Create from global state");
            this.state = global.state.StartScreen;
        } else {
            logi("Create from local state");
            global.state.StartScreen = this.state;
            this.state.ctlbtn.click = "start";
            this.state.callbtn.click = "call";
        }

        this.api = window.livechat2Api;

        this.fn = {start: this.start, end: this.end, call: this.call, endcall: this.endcall};
    };

    oneClick = () => {

        if (this.api.url !== configs.selected.name) {
            logw("url changed to : " + configs.selected.name);
            this.api = window.livechat2Api = new Livechat2Api(configs.selected.webSocketUrl);
        }

        this.__createCallSession();

        var sclick = new SingleClickExample(
            window.livechat2Api,

            global.callsession,

            (sid) => {
                this.setState((s) => {
                    s.userid = sid;
                    s.reqbtn = {isActive: true};
                    return s;
                });
            },

            this.onStartFailed,

            (content) => {
                logi("reqAgent : " + content.userId);
                this.setState((s) => {
                    s.findUserResp = content;
                    s.callbtn = {txt: "Call", isActive: true, click: "call"};
                    return s;
                });
            }
        );

        var cobj;
        try {
            cobj = JSON.parse(this.state.rqparam)
        } catch (err) {
            loge("Invaid request param json");
            return;
        }
        ;

        var cobj = Object.assign(
            {
                Authentication_Status: 'Verified', Gender: 'Male', Language: 'English',
                Mobile: '9988', URL: 'accounts@IDFC.com', channel: 'chat', id: '212212',
                intent: 'account', name: 'Nuwan'
            }, cobj);

        sclick.CallVideo(this.state.rqskill, cobj);
    }

    __createCallSession = () => {
        global.calls.createCall();

        global.callsession.AddHandler(global.callsession.StateEvent.CONNECTING_MEDIA, "start",
            (mode) => {
                global.pushNav(this.props.navigation, "Call");
            }
        );

        global.callsession.AddHandler(global.callsession.CommonEvent.CALL_TIME, "start",
            (time, fmttime) => {

            }
        );

        global.callsession.AddHandler(global.callsession.StateEvent.ENDED, "start",
            (mode) => {
                this.props.navigation.navigate('start');
            }
        );
    }

    setUserAuth = () => {

        let userId = Math.floor(Math.random() * 1000000).toString();
        let authToken = "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.FjuQDR8jXZvVQRQnDgDucbUrf8dIk7DuQaXAoKfXw9UbOEFjdsZpY-PRj0_Y88OU5nJbtuKwrU7klL1f0Oe964OFAzdbgQUovI7NLEGeMr1XI0TVSOEDcOWt3j3n2_Vh3mGGOmIzyQ7FW6OlnFOHitYF1jk2073fAIN0ey0yOVgxpuw7bvDbO154IP9wcE5PTD8EU1FG9VLhYKzLxwelG7j09bceS7kL5qjQKoJmnpWt24lgKaF7yDChQp3jHLz2yE6JIuMXpVy3LsXOxbIxGLuiyIsh0N7Bd7CopIx82DBCFPqVNH8x8dhAOpYO7oTL_dH3wG5pVpBeOrztxHXO5Q.-OdhCsH7mhp-jJpK.tdeSo5zItdkjDL5PsX93U3DsNhjPOt12u2vNpKuLI71uS4-vgNsMlKJjKtoXP_1ZMlNTjOczjDr8qgX0_9_m9mud_BC2zL5AwrO2j5daAoglDRG1b2Xfv_z9JtfEnzF7ClOiWX9poHAa-vfPx8JUhb_U0l3rDTzMixnDuTkZAAALU6OAtmTEiPvcaVXoO1JWjPbE1qPJt6bli1EoSCJFCO1IwviHtFufufbJx_2ZPYT5WPRJIFXVcwAMtaayLwLpYhle06iDEBAwHv3wpCgo6VmHmkrrImRb6ZDA2CXC_RA5Rj3UjSrkubbr6noLZdO1h5UkF_00aWn5cA.rUW7bqBgmOio_TS9B5bwmg";
        let attributes = {};
        attributes["sessionId"] = "3a600342-a7a3-4c66-bbd3-f67de5d7096f";
        attributes["authType"] = "idfc"; //Possible values are idfc, cloud

        // Alert.alert("invoke  SDK setUserAuth ");
        this.api.setUserAuth(userId, authToken, attributes);
        // Alert.alert("  SDK setUserAuth invoked successfully");
    }

    start = () => {
        if (this.api.url !== configs.selected.name) {
            logw("url changed to : " + configs.selected.name);
            this.api = window.livechat2Api = new Livechat2Api(configs.selected.webSocketUrl);
        }


        logi("starting.. start screen idfc");


        this.__createCallSession();

        /* Doesn't work

        global.clickChat = () => {
            global.pushNav( this.props.navigation, "Logs" );
        };
        */

        this.api.onStartSuccess = (sid) => {
            //   this.setState(s => {
            //       s.userid = sid;
            //       return s;
            //   });
            //  this.api.livechat.setUserId(sid);
            //  console.log("onStartSuccess : " + sid);
            //
            // //logi("onStartSuccess : " + sid);
            //   this.setState((s) => {
            //       s.reqbtn = {isActive: true};
            //       return s;
            //   });
            //   global.callsession.Initialize(sid); // userid == sid

            let userId = this.api.livechat.getUserId();
            if (userId == undefined || userId == "") {
                userId = sid;
            }
            this.setState(s => {
                s.userid = userId;
                return s;
            });
            this.api.livechat.setUserId(userId);
            logi("onStartSuccess : " + sid);

            this.setState((s) => {
                s.reqbtn = {isActive: true};
                return s;
            });

            global.callsession.Initialize(userId); // userid == sid
        };

        this.api.onEnd = (r) => {
            logi("onEnd : " + r);
        }

        this.api.onStartFailed = (error) => {
            logi("onStartFailed : " + error);
        }

        this.api.onUserMessage = (message, from) => {

            logi("onUserMessage : " + message + ", from : " + from + "*");
            var msgobj = JSON.parse(message);

            this.setState((s) => {

                s.txtList.push({
                    type: 'recv', 'index': s.txtList.length,
                    msg: msgobj.message_content
                });

                return s;
            });
        }

        this.api.init();
        //this.api.livechat.setUserId( (Math.floor ( Math.random() * 10000) + "" ) );

        this.setUserAuth();

        this.api.start({name: "Nuwan"}, true);

        this.setState((s) => {
            s.ctlbtn = {txt: "End", isActive: true, click: "end"};
            return s;
        });

    }

    end = () => {
        logi("ending..");
        //global.callsession.End( "End by App" );
        window.livechat2Api.end();
        this.setState((s) => {
            s.ctlbtn = {txt: "Start", isActive: false, click: "start"};
            return s;
        });
    }

    call = () => {
        logi("call..");
        this.setState((s) => {
            s.callbtn = {txt: "End Call", isActive: true, click: "endcall"};
            return s;
        });

        logi("Call from :" + this.state.userid + ", to :" + this.state.findUserResp.interactionId);
        global.callsession.StartAV
        (
            "video",
            {
                id: this.state.findUserResp.userId,
                remoteAddr: ("interaction:" + this.state.findUserResp.interactionId),
                title: "Test User"
            },
            false
        );
    }

    endcall = () => {
        logi("ending call..");

        this.setState((s) => {
            s.callbtn = {txt: "Call", isActive: true, click: "call"};
            return s;
        });

        global.callsession.End("End by click");
    }

    //let requestId =  global
    clickLog = () => {
        global.pushNav(this.props.navigation, 'Logs');
    }

    // { intent:"", reqSkill:"", reqId:"", type:"", interactionId:"", channel:"", userDetails:"", userId:"" }
    onUserFound = (content) => {
        logw("onUserFound : " + JSON.stringify(content));

        this.setState(s => {
            s.findUserResp = content;
            s.agentUserId = content.userId;
            s.callbtn = {txt: "Call", isActive: true, click: "call"};
            return s;
        });

        logw("onUserFound : " + content.userId);
    }

    onUserNotFound = (content) => {
        this.state.isFoundEWTStatus = true;
        this.setState(s => {
            s.callbtn = {isActive: false};
            return s;
        });
    };

    reqAgent = (skill) => {
        logw('==== fire req agent....');
        this.api.onUserFound = this.onUserFound;
        this.api.onUserNotFound = this.onUserNotFound;
        this.api.onRequestQueued = this.onRequestQueued;

        var obj;
        try {
            obj = JSON.parse(this.state.rqparam);
        } catch (err) {
            loge("Invaid request param json");
            return;
        }

        let requestId = Math.floor(Math.random() * 1000000).toString();

        this.setState((s) => {
            s.requestId = requestId;
            return s;
        });

        let id = '32212';
        let name = 'Damith Premakumara';
        let __intent = 'banking'; //banking //135197
        let __channel = 'banking'; //banking //chat7
        let language = "English";
        let pageDesc = "Accounts";
        let authentication = true;
        let mobileNo = "919988774321";
        let customerInfo = {id, name, mobileNo, pageDesc, authentication};

        this.api.findUser(requestId, '', __intent, __channel, language);

        logw('==== fire End req agent....');

        //  findUser = (requestId, customerInfo, intent, channel = "chat", language = null)
        // this.api.findUser(rqid,
        //     JSON.stringify(
        //         Object.assign(
        //             {
        //                 Authentication_Status: 'Verified', Gender: 'Male', Language: 'English',
        //                 Mobile: '9988', URL: 'accounts@IDFC.com', channel: 'chat', id: '212212',
        //                 intent: 'account', name: 'Nuwan'
        //             }, obj)
        //     ),
        //     skill
        // );
    }

    toSettings = () => {
        this.props.navigation.navigate('Settings');
    }

    sendMessage = (text) => {
        logd("User text = " + text);
        this.api.sendUserMessageV2(text, ("interaction:" + this.state.findUserResp.interactionId));

        this.setState((s) => {
            s.txtList.push({
                type: 'sent', 'index': s.txtList.length,
                msg: text
            });
            s.userText = "";
            return s;
        });
    }

    _renderItem = ({item, index}) => {

        var istyle = {fontSize: 15, color: 'blue'};

        var bgcolsel = item.type;

        istyle.backgroundColor = this.itemBgcolor[bgcolsel];

        return <Text style={istyle}>{item.msg}</Text>;
    }


    onRequestQueued = ({item, index}) => {
        this.state.isFoundEWTStatus = true;
        logi("On Request Queued ");
    };

    getEWTStatus = (id) => {
        try {
            logd("requestId: " + this.state.requestId);
            this.api.getEWTStatus(this.state.requestId);
            this.api.onEWTStatus = this.onEWTStatus;
        } catch (e) {
            loge("Error in getEWTStatus: ", e);
        }
    };

    onEWTStatus = (obj) => {
        logw("Ewt status========");
        logi(JSON.stringify(obj));
        this.state.EWTStatus = JSON.stringify(obj);
        console.log("Ewt status========");
    };


    clickCallTest = (obj) => {
        logi("===== Test call======");
        //global.calls.createCall();
        global.pushNav(this.props.navigation, "Call");
    };


    render() {
        console.log("Rendering StartScreen ...");

        var callbtn, reqbtn, textchat;

        callbtn = (<View>
                <AppButton title={this.state.callbtn.txt}
                           onPress={this.fn[this.state.callbtn.click]}/>
            </View>
        );

        textchat = (
            <View>
                <View flexDirection='row'>
                    <TextInput multiline={true}
                               style={Object.assign({width: 300, height: 60}, this.styles.textinput)}
                               onChangeText={(newtext) => {
                                   this.setState((s) => {
                                       s.userText = newtext;
                                       return s;
                                   });
                               }
                               } value={this.state.userText}></TextInput>

                    <AppButton title={"Send"} onPress={() => {
                        this.sendMessage(this.state.userText);
                    }}/>
                </View>
                <View style={{flex: 0.4}}>
                    <FlatList style={{backgroundColor: 'lightyellow', width: 300, height: 100}}
                              data={this.state.txtList} extraData={this.state.txtList.length}
                              renderItem={this._renderItem}/>
                </View>
            </View>
        );

        if (this.state.reqbtn.isActive) {
            reqbtn = (
                <View>
                    <TextInput style={Object.assign({height: 40, width: 300}, this.styles.textinput)}
                               value={this.state.rqparam}
                               onChangeText={(v) => {
                                   this.setState((s) => {
                                       s.rqparam = v;
                                       return s;
                                   });
                               }}></TextInput>
                    <View style={{flexDirection: 'row'}}>
                        <AppButton title="Request agent" onPress={() => {
                            this.reqAgent(this.state.rqskill);
                        }}/>
                        <TextInput style={Object.assign({textAlign: 'center'}, this.styles.textinput)}
                                   value={this.state.rqskill}
                                   onChangeText={(v) => {
                                       this.setState(s => {
                                           s.rqskill = v;
                                           return s;
                                       });
                                   }}></TextInput>

                    </View>
                </View>
            );
        }

        return (

            <View style={{
                backgroundColor: 'white',
                flex: 1,
                marginTop: 50,
                flexDirection: 'column',
                alignItems: "center",
            }}>
                <View>
                    <AppButton title={this.state.ctlbtn.txt} onPress={this.fn[this.state.ctlbtn.click]}/>
                </View>

                <AppButton title="Start once" onPress={this.oneClick}/>

                {reqbtn}

                {callbtn}

                <View>
                    <AppButton title="Log" onPress={this.clickLog}/>
                </View>

                {/*<View>*/}
                {/*    <Text>{this.state.EWTStatus}</Text>*/}
                {/*</View>*/}


                <View>
                    <AppButton title="getEWTStatus" onPress={this.getEWTStatus()}/>
                </View>

                <View>
                    <AppButton title={"Env : " + configs.selected.name} onPress={this.toSettings}/>
                </View>


                {textchat}

                <View style={{alignItems: 'flex-start', width: 300, backgroundColor: 'lightblue'}}>
                    <Text style={{color: "blue", fontSize: 12, margin: 2}}> my userid: {this.state.userid}</Text>
                    <Text style={{color: "blue", fontSize: 12, margin: 2}}> agent userid: {this.state.agentUserId}</Text>
                    <Text style={{color: "blue", fontSize: 12, margin: 2}}> IDFC TestApp: July-8th-15pm</Text>
                    <Text style={{color: "blue", fontSize: 12, margin: 2}}> livechatav: v{this.state.libver}</Text>
                </View>

            </View>

        );

    }

    /*

        type : 'audio' | 'video'

        */
    startWithAv = (type) => {
        this.start();
    }

    styles = {
        textinput: {
            color: 'blue',
            backgroundColor: 'lightyellow',
            fontSize: 15,
            minWidth: 100,
            minHeight: 20,
            borderColor: 'gray',
            borderWidth: 1,
            borderRadius: 5
        }
    }

}

