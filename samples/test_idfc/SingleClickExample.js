
import {Livechat2Api} from 'react-native-livechatav/sig/livechat2api';

function logi(msg){
    global.log.func("info", msg);
    return msg;
}

function logw(msg){
    global.log.func("warn", msg);
    return msg;
}

function loge(msg){
    global.log.func("error", msg);
    return msg;
}

function logd(msg){
    global.log.func( "debug", msg);
    return msg;
}

/**
 *
 * This is an example code which shows how to start a video call with a single function call.
 *
 *
 * Usage :
 *
 * var api = new Livechat2Api( configs );
 * var callSession = new CallSession( { SigUrl:"wss://example.com", CustInfo:"" } );
 *
 * // set callSession event handlers
 *
 * // pass Livechat2Api event handlers into 'SingleClickExample' objects
 *
 * var sclick = new SingleClickExample( api, callSession, onStartSuccess, onStartFail, onAgentConnect )
 *
 * sclick.CallVideo( "Banking", { name:"Sam" } );
 *
 * // To end everything with a single call
 *
 * sclick.End( "User ended" );
 *
 */

export default class SingleClickExample
{
    constructor( api, callSession, onStartSuccess, onStartFail, onAgentConnect )
    {
        window.livechat2Api = api;
        this.api = api;
        this.callSession = callSession;
        this.onStartSuccess = onStartSuccess;
        this.onAgentConnect = onAgentConnect;
        this.onStartFail = onStartFail;
    }

    setUserAuth = () =>
    {

      let userId = Math.floor(Math.random() * 1000000).toString();
      let authToken = "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.FjuQDR8jXZvVQRQnDgDucbUrf8dIk7DuQaXAoKfXw9UbOEFjdsZpY-PRj0_Y88OU5nJbtuKwrU7klL1f0Oe964OFAzdbgQUovI7NLEGeMr1XI0TVSOEDcOWt3j3n2_Vh3mGGOmIzyQ7FW6OlnFOHitYF1jk2073fAIN0ey0yOVgxpuw7bvDbO154IP9wcE5PTD8EU1FG9VLhYKzLxwelG7j09bceS7kL5qjQKoJmnpWt24lgKaF7yDChQp3jHLz2yE6JIuMXpVy3LsXOxbIxGLuiyIsh0N7Bd7CopIx82DBCFPqVNH8x8dhAOpYO7oTL_dH3wG5pVpBeOrztxHXO5Q.-OdhCsH7mhp-jJpK.tdeSo5zItdkjDL5PsX93U3DsNhjPOt12u2vNpKuLI71uS4-vgNsMlKJjKtoXP_1ZMlNTjOczjDr8qgX0_9_m9mud_BC2zL5AwrO2j5daAoglDRG1b2Xfv_z9JtfEnzF7ClOiWX9poHAa-vfPx8JUhb_U0l3rDTzMixnDuTkZAAALU6OAtmTEiPvcaVXoO1JWjPbE1qPJt6bli1EoSCJFCO1IwviHtFufufbJx_2ZPYT5WPRJIFXVcwAMtaayLwLpYhle06iDEBAwHv3wpCgo6VmHmkrrImRb6ZDA2CXC_RA5Rj3UjSrkubbr6noLZdO1h5UkF_00aWn5cA.rUW7bqBgmOio_TS9B5bwmg";
      let attributes = {};
      attributes["sessionId"] = "3a600342-a7a3-4c66-bbd3-f67de5d7096f";
      attributes["authType"] = "idfc"; //Possible values are idfc, cloud

      // Alert.alert("invoke  SDK setUserAuth ");
      this.api.setUserAuth(userId, authToken, attributes);
     // Alert.alert("  SDK setUserAuth invoked successfully");
    }

    __callav = ( skill, custDetailsObj, type ) =>
    {
       logi( "starting.. single click" );

       var __onUserFound = ( content ) =>
       {
           logi( "onUserFound : " + JSON.stringify( content ) );
           this.onAgentConnect( content );

           this.callSession.StartAV(
               type,
                {
                    id : content.userId,
                    remoteAddr : ( "interaction:" + content.interactionId ),
                    title : "Test User"
                },
               /*{ id: content.userId , title:"Agent name" },*/
                false
           );
       };

       var __reqAgent = ( skill, custDetailsObj ) =>
       {
           this.api.onUserFound = __onUserFound;
           var rqid = Math.floor( ( Math.random() * 10000) + "");

           //  findUser = (requestId, customerInfo, intent, channel = "chat", language = null)
           this.api.findUser( rqid, JSON.stringify( custDetailsObj ), skill );
       }

       this.api.onStartSuccess = ( sid ) =>
       {
          // this.api.livechat.setUserId( sid );
          // logi( "onStartSuccess : " + sid );
          // this.callSession.Initialize( sid ); // userid == sid
          // this.onStartSuccess( sid );
          //
          // __reqAgent( skill, custDetailsObj );

           let userId = this.api.livechat.getUserId( );
           if(userId == undefined || userId == ""){
               userId = sid;
           }
           this.api.livechat.setUserId( userId );
           logi( "onStartSuccess : " + sid );
           this.callSession.Initialize( userId ); // userid == sid
           this.onStartSuccess( sid );

           __reqAgent( skill, custDetailsObj );
       }

       /*
       this.api.onEnd = (r) =>
       {
           logi( "onEnd : " + r );
       }
       */

       this.api.onStartFailed = ( error ) =>
       {
          logi( "onStartFailed : " + error );
          this.onStartFail( error );
       };

       this.api.init();

       this.setUserAuth();

       this.api.start( custDetailsObj, true );

    }

    CallVideo = ( skill, custDetailsObj ) =>
    {
        this.__callav( skill, custDetailsObj, 'video' );
    }

    End = ( reason ) =>
    {
        this.callSession.End( reason );
        this.api.end( reason );
    }

}


