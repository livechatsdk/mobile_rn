import {StyleSheet} from 'react-native';

const consts = require('../constants/common');

export const styles = StyleSheet.create({
    btnEndCall: {
        aspectRatio: 1,
        width: 60,
        height: 60,
        alignContent: 'center',
        alignItems: 'center',
        borderWidth: 10,
        borderColor: 'white',
        borderRadius: 100,
        backgroundColor: 'white',
        flex: 1,
    },
    callEndIcon: {
        width: 70,
        height: 70,
    },
    textChatArea: {
        width: '100%',
        height: 60,
    },
    mainViewWrapper: {
        flex: 1,
        marginTop: 30,
        padding: 15

    },
    mainMenuWrapper: {
        width: '100%',
        height: 60,
        borderWidth: 2,
        borderColor: consts.COLORS.GREY_100,
        alignContent: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginBottom: 10,
        paddingLeft: 20
    },
    mainWrapper: {
        alignItems: 'center'
    },
    menuTextWrapper: {
        fontWeight: '800',
        color: consts.COLORS.BLACK,
        fontSize: 15,
        alignContent: 'center',
    },
    headerWrapper: {
        flexDirection: 'row',
        marginBottom: 30
    },
    requestFormWrapper: {},
    requestBodyWrapper: {
        margin: 15,
        borderTopWidth: 1,
        borderTopColor: consts.COLORS.GREY_100,

    },
    txtWrapper: {
        flexDirection: 'row'
    },
    btnStyle: {
        width: 200,
        height: 50,
        alignContent: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: consts.COLORS.BG,
        borderRadius: 10
    },
    btnTextStyle: {
        color: consts.COLORS.WHITE,
        fontSize: 15,
    },
    headerTextStyle: {
        color: consts.COLORS.GREY_400,
        fontSize: 20,
        marginBottom: 20,
        fontWeight: '200',
        paddingTop: 25
    },
    btnStyleWrapper: {
        alignContent: 'center',
        marginTop: 20
    },
    textInput: {
        color: consts.COLORS.BLACK,
        fontWeight: '200',
        fontSize: 18,
        height: 50,
        borderColor: consts.COLORS.GREY_100,
        borderWidth: 2,
        borderRadius: 5,
        width: 370,
        marginBottom: 10,
        paddingLeft: 10
    },

    textTypingInput: {
        color: consts.COLORS.BLACK,
        fontWeight: '200',
        fontSize: 18,
        height: 50,
        borderColor: consts.COLORS.GREY_100,
        borderWidth: 2,
        borderRadius: 5,
        width: 270,
        marginBottom: 10,
        paddingLeft: 10
    }
});
